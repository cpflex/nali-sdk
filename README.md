﻿# Cora Flex CT1XXX Series SDK 

This package contains the documentation, CP-Flex application scripts, and tools 
for working programming the CT1XXX series Flex trackers.

[Click here for release notes](./ReleaseNotes.md)

![tag](flex/images/Nali_100.png)

## Overview
The SDK provides a collection of the various tools and script applications for programming
the N100 Tag.  This SDK is intended for Codepoint partners during development to facilitate testing
and device programming.
		
## SDK Contents
The SDK is currently light on documentation other than what is described above.  The primary means
for communicating what is required is by example.   

The SDK is organized into three sections

* [**firmware**](firmware/README.md) – Recent firmware binaries for development kits
* [**tools**](tools/README.md) – Tools for programming the Cora CT1XXX series trackers.

## Prequisites

SDK has been updated with a new programmer eliminating dependencies on other packages.


---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
