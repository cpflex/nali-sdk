## Nali100 Overview
The Nali100 is a low power, credit card size, indoor and outdoor location device. The Nali100 can be used as an enterprise asset tracking device, an employee safety badge or a student card with emergency button. The Nali100 uses the LoRaWAN protocol to provide seamless and reliable campus wide coverage. Nali is designed for high security, long battery life and seamless indoor/outdoor location. Users can set the frequency of location requests from once per 5 minutes to once per month through App.

The LoRaWAN protocol is highly secured communication and uses two layers of cryptography: 

* A unique 128-bit Network Session Key shared between the end-device and network server  
* A unique 128-bit Application Session Key (AppSKey) shared end-to-end at the application level  

AES algorithms are used to provide authentication and integrity of packets to the network server and end-to-end encryption to the application server. By providing these two levels, it becomes possible to implement multi-tenant shared networks without the network operator having visibility of the user’s payload data. The keys can be Activated By Personalization (ABP) on the production line or during commissioning, or can be Over-The-Air Activated (OTAA) in the field.    
## Product Applications 

* Hotel Staff Management 
* Student Safety Tracker 
* Retirement Home, Hospital, Government Agency Safety Tracker 
* Contract Staffing Tracking 
* Enterprise Asset Tracking and Warehouse Asset Management 

## Functions 

* LoRa based communication 
* Supports LoRaWAN protocol 
* No gateway paring required
* Wi-Fi and BLE for indoor and outdoor 3-D positioning
* BLE for provisioning, log download, and firmware updates 
* Accelerometer for motion and fall detection 
* Rechargeable battery 
* Long battery life: 2+ months (reporting every 10 mins) 
* 36 months battery life if reporting position once per day 
* 1-Wire® Extensibility 
* Powerful, easy to configure script application environment (CP-Flex™) 
* Do not need to learn embedded firmware 
* 600+ message cache when offline 
* Two programmable buttons and two programmable LED indicators 
* IP67 rating 
* Size: 86 x 54 x 4.8mm 

## Nali Physical Layout

![Nali100 Tag](./images/nali100Overview.png)

The above image shows the physical layout of the Nali100 tag, with the locations of the two buttons, LEDs and charging port shown.

## System Architecture

![Nali System Architecture](./images/systemArchitecture.png)

The Nali100 supports a variety of system implementations supporting both cloud- and enterprise-based deployments.  Systems integrators developing solutions using the Nali tag and related services can work with their preferred LoRaWAN network, readily integrating the additional tag and message translation services through easy-to-use web-services.  Nali tags communicate with the customer tracking solution via a deployed LoRa infrastructure (comprising multiple gateways and network management). Messages are encoded to maximize transport efficiency improving the battery life of the tag.  CodePoint-Flex cloud services are provided to facilitate message translation, provisioning, and location services. 

## Codepoint-Flex Services

CP-Flex™ services enhance the Nali Tag application flexibility, provide maximum performance and long battery life. CP-Flex™ Services provide message translation, indoor/outdoor location, provisioning, and application registration.

![CP-Flex Services](./images/cp-flexServices.png)

* Message Translation API – Nali messages are encoded to minimize transport data requirements.  The Message Translation API enables Customer Tracking Solutions to readily encode and decode messages providing a simple and intuitive JSON based web API for web application developments.
* Indoor / Outdoor Location – The Nali tag implements location determination capabilities at its core such that no matter the message content, the most-recent known location and time is always reported.  Location data like time tags are essential metadata and the service infrastructure provides the best estimate of location given measurement data collected by the tag both indoors and outdoors.  Indoor location can be further enhanced through probe and survey mechanisms depending on customer requirements.
* Tag Provisioning API – simplifies the process of provisioning and managing large-numbers tags by aggregating the tag provisioning process, simplifying the complicated activation and update activities involving multiple services.  Solution providers can use the provided CP-Flex™ provisioning tools for Android/iOS or integration the functionality into their own mobile applications.  
* Application Registrar API – Using the CP-Flex™ SDK, solution providers can develop custom script applications and transport encodings, which are then registered using the Application Registrar API to enable robust message translation and reporting. 

## Application Development

The Nali tag is an easily programmable device designed support a wide variety of applications: whether it be tracking people, animals, or “things”.  The CP-Flex™ Platform provides a scripting engine to control the tag’s behavior.  All activities in the tag are event driven, simplifying user code focusing on event handling rather than setting up complicated polling and state management.  

![Application Design](./images/appDesign.png)

The platform efficiently manages wake and sleep states, communications, and other functions notifying the script when changes occur.  Using the script capabilities, developers have full access to the device’s functions without having to deal with complex embedded firmware.

## Customer Application Structure

Tag Customer Applications are comprised of three elements as shown below.

![Application Structure](./images/appStructure.png)

* Object Specification – Each application defines an Object Specification file, which defines the key elements, identifies, and encoding mappings.  Application developers need only provide those object definitions for which they need to communicate.   The specification makes it possible for CP-Flex services to properly encode and decode raw message data.
* PAWN Script – events within the device are managed within the tag using handlers implemented in PAWN script.  PAWN is a highly efficient scripting engine that requires very little overhead to execute user functionality without adversely affecting power consumption. 
* Target Specifications – Customer applications are targeted to specific tag classes, device hardware revisions and firmware versions.  The target specifications enable developers to control, which devices are supported and allowed to run the specified application.  This information is stored as part of the application and maintained via Application Registrar API. 
Using the CP-Flex™ SDK, customer applications are compiled, validated, and registered with the Application Registrar API prior to uploading onto a Nali Tag.  Once registered, the application can be selected during the provisioning process for a particular tag application.  
Solution providers creating tag applications can start with the base applications provided in the SDK, which demonstrate using the various features and capabilities of the tag.

## CP-Flex™ Platform

The platform exposes the device functionality in the following modules.  As the device platform matures, these APIs will be periodically updated with new capabilities and features, which will be documenting in the SDK.

* Cache / Config – Save messages and configuration data in either short-term non-volatile memory or long-term data archive.  Script can use the caching functionality to store information that is unaffected by power management function.
* Communication – Transmit and receive encoded messages supporting application define payloads as well as standard payloads implemented by the Platform.  Data may be communicated to other services via LoRa or cached for later download via BLE (as application needs).
* LEDs / Buttons – Control the behavior and the meaning of the 4 available LEDS and two push buttons.  The Kiata Tag has two bi-color LEDs and push buttons that recognize short-press, long-press, continuous, and double-press events.
* Motion Sensing – Configure and receive motion events.  Motion sensor sensitivities and thresholds can be configured to support detection of a variety of motions including fall detection, crash detection, continuous motion, and no motion.   
* Positioning – Acquire and report positioning measurement data including Wi-Fi and BLE observations. Position is calculated by the Kiata-Flex location service.  As required the device may request location reports.
* Power Manager – Configure thresholds and monitor power related events including battery-low, power connect, and power disconnect.  Script can also control power consumption by enabling and disabling various board functions.
* RTC Alarms – Configure UTC based or relative alarms to control devices behavior over time.  Immediate timers can also be defined to manage short-term timeouts, which will expire before the device goes back to sleep.
* Sensors – Configure event threshold on available sensors (includes tag temperature) to receive updates when the sensor value exceeds some predefined threshold.


---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*

