# Provisioning Tags with the Linkware Services

[Easylinkin](https://easylinkin.net) is Codepoint's partner for providing LoRa backhaul services for the Nali100 tag. In order to use their services, you will require a Linkware LoRa gateway. The gateway has needs to be activated on the Linkware network, and you must obtain an account from Easylinkin.

Use the login credentials login to the Linkware site [here](http://lpwan.net4iot.com/cs/index).

Before registering a tag, you must configure the Linkware services where to route messages when they are received. This is done by configuring the company settings from the gear icon in the top right of the window. 

![Create Company](./images/createCompany.png)

The company information portion of the screen is self-explanatory. The 'Push' configuration configures Linkware for forwarding messages received from your devices to a tracking application. If you have developed your own tracking applicaiton, you should configure the Push settings to match the web service you have deployed. For the purposes of this document, we will configure the company to route messages to the Demo Locator app.

For the 'Push Configuration' portion, the following values should be entered:

- Push Type: HTTP

Once you enter HTTP, two addtional fields will appear, URL and Port.

- URL: Enter the URL of the default locator app: http://test.codepoint.services/cpflex/demo/locator/v1.0/linkware/submit?x-api-key=\<api_key\>. 
 For \<api\_key\>, we recommend entering the company name, e.g. companyabc. You will need this again later when configuring the Locator app.
- Port: 80
- Push Status: Normal
- Token, Send Number should be left blank
- Push Gateways: no
- Retransmission: Not Retransmission
- Heartbeat State: Freeze
- Alarm State: Freeze
- TimeOut Push: Freeze
- Signature Status: no

Click 'Save' to save the new company settings.

## Registering Tags

To register a tag, click on the 'Node Registration' link under the Nodes tab on the left of the screen. You will see a screen as shown below:

![Node Register](./images/nodeRegister.png)

Login Mode, Protocol Version, Class\_Type should all be left unchanged. You will need the AppEUI, DevEUI and AppKey of each tag you need to register. If a printout of this information was not provided with the tags, you can read the information from a tag using the Android provisioning tool (described in more detail [here](./NaliProgrammingInstructions.md). 

Enter the the 3 identifiers in the appropriate fields, making sure there are no spaces, dashes('-') or colons(':') between any of the digits.

For the RFRegion dropdown, select the correct region that corresponds to the tag you are using:

![RF Region](./images/RFRegion.png)

The only valid regions as of this writing would be US\_902\_928 and CN\_470\_510. It is critical that you select the correct RF region or your tag will not work.

The ADR\_Switch field should be changed from its default of ADRClose to ADR\_on. This will open two addtional fields, ADR\_DR\_Slow and ADR\_DR\_Fast which should both be left unchanged. The last field to complete is the Timeout Period, which should have a value of '-1' (yes, minus one).

Click 'Submit' and should see a success message. The tag is now registered with the Linkware LoRa network and able to send and receive messages.


---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*


