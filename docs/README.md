# Nali100 Tag SDK
This README gives a complete overview of the use and deployment of the Nali100 Locator Tag. 

The Nali tag is a low power, credit card sized device designed for locating and tracking persons and things. It is a small, battery powered device useful for tracking and locating indoors and out. A complete suite of backend services are provided to ease the largescale deployment of tags for Enterprise tracking purposes. This SDK focuses primarily on application development for the tag itself, the [companion SDK](https://gitlab.com/kiata/partners/flex-api-sdk) focuses on backend development of locator applications and services. 

1. [Overview of Nali Tags](./NaliOverview.md).
3. [Provisioning Tags for Linkware Services](./LinkwareProvisioning.md)
4. [Using the Default Example Locator App](./defaultLocator.md)
