# Using the Default Instance of the Locator App

**NOTE:  THIS DOCUMENTATION IS OUT OF DATE**


The [Flex API SDK](https://gitlab.com/kiata/partners/flex-api-sdk) is used to create your own backend locator services for the Nali tag. In order to quickly demonstrate Nali functionality, Codepoint has provided an example web app to showcase locator functions for the Nali100 tag.

Full source code is provided for both the web server and web app, and we strongly encourage you to start with this source code and create your own showcase locator app. It is packaged in a Docker container, making deployment fast and simple.

When we provisioned the tags on Linkware earlier, we set up the default URL to route all received messages from Nali tags. You can also set up 'sub-companies' that will route received messages to an addtional web service over and above the default setting for the master company. This provides addtional flexibility if you are developing your own locator applications.

To access the default instance of the Locator app, click on the following link:
[http://test.codepoint.services/cpflex/demo/locator/v1.0/app/index.html](http://test.codepoint.services/cpflex/demo/locator/v1.0/app/index.html).

![Locator App](./images/locatorApp.png)

In the field for 'Account Key (api\_key)', you must enter *exactly* the same value that you specified in the URL string when you created the company. The Locator app will *only* show messages received that match the api\_key received in the message with that specified on the Locator app screen. Click on the 'Update' button to view messages received within the last hour.

![Locator Record](./images/locatorAppWithData.png)

The DevEUI and associated information about the positon report are shown when clicking on the position icon.

The web app is a simple javascript app using Leaflet for map display, so it is very easy to modify it to display different map providers, and customize the format of the pop-up displayed.

---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*
