﻿# LoRa Tag SDK Release Notes

### V0.10.6.0 21-03-11 ###

1) Updated Nali Programmer V1.4.0.0
2) Added Opl1000 v0.9.0.0a firmware builds
3) Added STM v0.10.0.0a firmware builds

### V0.10.5.1 210102 ###

1) Updated NaliMobileTools application (v0.7.2).
2) Pushed latest debug builds.

### V0.10.4.1 20108 ###

Please check the published packages for the latest firmwares.   Debug builds for firmware are available in the SDK.

1) Updated STM Firmware to fix reset issues and resource contentions when starting the Flex application.
2) Updated all Flex App Battery.p implementations to verify network is availble before sending status.
3) Added two new applications for Easylinkin Test #1 and #2 with 30 and 600 second tracking intervals.
   Readme files are included describing the simplified interface.
4) Consolidated STM firmware builds, going forward only the curent stable and latest versions will be provided.

### V0.10.3.1 200917 ###

Current Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_2_1_200909.bin
* STM FW: stmfw_CN470_rev3_0_9_4_0_200917.bin, stmfw_US915_rev3_0_9_4_0_200917.bin, and stmfw_EU868_rev3_0_9_4_0_200917.bin
* Scripts: 
  * Standard Nali-100 Tag Demo App: *tag_demo_v0_9_2_0_200915.bin*
  * Traxmate Demo App: *traxmatedemo_v0_5_0_1_200915.bin*
  * Pedestrian Tracker application: *ped_tracker_v0_9_1_4_200719.bin*

#### Change Log
1)  Updated Tag Demo to new specs provided by real-World demos.
2)  Added new Traxmate demo app to align with specifications provided by Traxmate.  Tried
    to make their use a bit simpler.
3)  Updated STM with production bug fix.

### V0.10.2.2 200909 ###
Releases updated Nali Programmer v1.3.1.1 and Android Tools V0.6.0.2  with support for CP-Flex hub downloads.  This is the last official release of firmware binaries.  Going forward, future releases will be posted directly to the CP-Flex Hub.

Current Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_2_1_200909.bin
* STM FW: stmfw_CN470_rev3_0_9_3_1_200713.bin, stmfw_US915_rev3_0_9_3_1_200713.bin, and stmfw_EU868_rev3_0_9_3_1_200713.bin
* Scripts: tag_demo_v0_9_1_2_200504.bin, and ped_tracker_v0_9_1_2_200504.bin

### V0.10.1.0 200719 ###
Releases updated Nali Programmer v1.2.1.0

Current Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_1_10_200717.bin
* STM FW: stmfw_CN470_rev3_0_9_3_1_200713.bin, stmfw_US915_rev3_0_9_3_1_200713.bin, and stmfw_EU868_rev3_0_9_3_1_200713.bin
* Scripts: tag_demo_v0_9_1_2_200504.bin, and ped_tracker_v0_9_1_2_200504.bin

### V0.10.0.0 200719 ###
This release implements the new CP-Flex Hub support in the Nali Programmer tools  Androind Mobile tools are not yet updated.  This includes new releases for all 
firmware.

Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_1_10_200717.bin
* STM FW: stmfw_CN470_rev3_0_9_3_1_200713.bin, stmfw_US915_rev3_0_9_3_1_200713.bin, and stmfw_EU868_rev3_0_9_3_1_200713.bin
* Scripts: tag_demo_v0_9_1_2_200504.bin, and ped_tracker_v0_9_1_2_200504.bin

1) Updated OPL Firmware
2) Updated STM Firmware
3) Updated Nali Programmer


### V0.9.2.0 200622 ###
This release fixes some issues with the buttons and adds RF test script.

Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_0_4_200325.bin
* STM FW: stmfw_CN470_rev3_0_9_2_4_200622.bin, stmfw_US915_rev3_0_9_2_4_200622.bin, and stmfw_EU868_rev3_0_9_2_4_200622.bin
* Scripts: tag_demo_v0_9_1_2_200504.bin, and ped_tracker_v0_9_1_2_200504.bin

1) Fixed button issues caused when pushing both buttons simultaneously.  Was causing inconsistent behavior.
2) Added CP-Flex app for Changhong RF testing.

### V0.9.1.3 200504 ###
This release fixes a number of stability issues with the V0.9.0.  Be sure to update
both STM firmware and flex applications to the V0.9.1.X release versions to ensure compatibility.

Firmware sets:
* OPL FW: opl1000_ota_a2_rev3_0_8_0_4_200325.bin
* STM FW: stmfw_CN470_rev3_0_9_1_3_200504.bin, stmfw_US915_rev3_0_9_1_3_200504.bin, and stmfw_EU868_rev3_0_9_1_3_200504.bin
* Scripts: tag_demo_v0_9_1_2_200504.bin, and ped_tracker_v0_9_1_2_200504.bin

1) Fixed network rejoin bug, preventing tag from attempting to rejoin when out of coverage or previous attempts failed.
2) Added support for confirmed and unconfirmed MCPS messages.   
3) Implemented support for prioritized messages
4) Added network coverage functions, see tag_demo and ped_tracker readme.

### V0.9.0.1 200413 ###
This release has provides support for data downlink to tags.  v0.9 scripts and STM firmware are not interchangeable with previous versions.
Please be sure to load v0.9 STM firmware and scripts.

* OPL FW: opl1000_ota_a2_rev3_0_8_0_4_200325.bin
* STM FW: stmfw_CN470_rev3_0_9_0_1_200413.bin, stmfw_US915_rev3_0_9_0_1_200413.bin, and stmfw_EU868_rev3_0_9_0_1_200413.bin
* Scripts: tag_demo_v0_9_0_1_200410.bin, and ped_tracker_v0_9_0_1_200410.bin

1) Implemented downlink support.  Read documentation updates to ped_tracker and tag_demo for more information. See
   also dcoumentation for Locator Demo service for details on implementing application downlink support.
2) Implemented TelemMgr in ped_tracker and tag_demo to automatically poll at 1hr intervals if no
   other messages have been sent during the interval.

3) Increased dynamic memory on tag to 2K RAM.  This improves buffering during transmit and receive operations.


### V0.8.0.4 200325 ###
This release has support for various positioning performance and technology.  This includes advance to v0.8.x versions of the STM and OPL firmware as well as updates to both perf_test and ped_tracker applications providing functionality to configure the positioning mode.  

* OPL FW: opl1000_ota_a2_rev3_0_8_0_4_200325.bin
* STM FW: stmfw_CN470_rev3_0_8_0_4_200325.bin, stmfw_US915_rev3_0_8_0_4_200325.bin, and stmfw_EU868_rev3_0_8_0_4_200325.bin

1) Implemented infrastructure to support BLE and WiFi based location measurements with options to select coarse, medium, and precise measurement modes.
2) Restructured OPL app to improve event reporting and maintenance. 
3) Scanning is currently not available when devices are connected to a charger.  This is due to BLE scanning and BLE GaTT functions not being available simultaneously.    Looking at options to relax this requirement later.
4) Updated perf_test and ped_tracker with support for changing postioning mode and sensor technologies.  See readme.md files for details.

### v0.7.0.4 - 200213 ###

This release has updates to OPL OTA, STM.  Be sure to install 
the latest versions, starting with the OPL first.   These releases are designed to work with the 
new Nali N100 BLE/USB Programmer.  **Do not use the Android App until we replace it.**

* OPL FW: opl1000_ota_a2_rev3_0_7_0_4_200211.bin
* STM FW: stmfw_CN470_rev3_0_7_0_1_200211.bin and stmfw_US915_rev3_0_7_0_1_200211.bin

1) KP-102 - Added improvements to support BVT test framework.
2) KP-96 - Reworked firmware updates to work using UART and BLE.
3) KP-120 - Fixed "bricked" STM behavior due to OPL Power being left on after charger off + Low/Depleted Battery Scenario).
4) KP-117 - Restructured Message framework to support UART and BLE Communications with same command processing infrastructure.
5) KP-113 Fixed OPL MAC address configuration reporting bug. 
6) Changed BLE name to Nali-N100.
7) KP-107  Added Charge Complete detection in both ped_tracker and perf_test applications.
8) Stabilized firmware programming between improved reliability.
9) Fixed BLE reconnection issue when updating OPL firmware.

### v0.6.2.3 - 200108 ###

This release has updates to OPL OTA, STM OTA, and perf_test.amx.bin  Be sure to install the latest versions, starting with the OPL first.   **OPL MUST BE INSTALLED FIRST OVER OTA OR RISK BRICKING**  

* OPL FW: opl1000_ota_a2_rev3_0_6_1_3_200102.bin
* STM FW: stmfw_CN470_rev3_0_6_2_7_200108_ota.bin and stmfw_US915_rev3_0_6_2_7_200108_ota.bin
* Script: perf_test_200102_ota.bin and ped_tracker_200108_ota.bin

1) Added ped_tracker_ces_200105.bin and ped_tracker_ces_ota_200108.bin  for higher rate reporting interval for CES and other Demos.
2) Added docs directory describing provisioning and other activities.
3) KP-107 - Updated power percetage reader to accurately report remaining power, handled hardware differences between Proto3 and Eval3.
4) KP-112 - Fixed improper formatting of WiFi measurement data.
5) KP-108 - Fixed overwrite of Mac addresses in OTA version.
6) KP-114 - Fixed STM bricking issue after OTA BLE Script update.
7) KP-118 - Updated STM binaries to include merged KP-108 code (it was missed in last build).
8) Updated PED tracker messages to set category and priority when emergencies happen.

### v0.6.1.1 - 191219 ###

This version implements stabilization tweaks and is being released after a 36 hour burn test running perf_test.   

1) Enhanced perf test with more diagnostic output.
2) Updated STM firmware to version 0.6.1.1 (both binary and OTA packed versions).
3) Updated LoRa Tag Programmer verion v1.0.1.0


### v0.6.1.0 - 191216 Release ###

This release updated both the STM_Firmware and LoRa tag programmer.   Be sure to pull
the latest versions.

Improvements and Bug Fixes:

1) KP-93  Fixes STM IO PC10 leakage issue.
2) KP-63  Fixes RTC Epoch 
3) Rework Software Configuration BLE request for more reliability.
4) KP-94  Fixes Alarm functions stmfw_xxxx_rev3_0_6_1_0_191216
5) Updated UART Programmer logo.
6) Regenerated OTA versions for STM and Perf_test.   
---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
