﻿# Nali N100 Firmware 

The latest versions of release firmware binaries can be found here.  These
versions are interim releases, which will ultimately be superseded by CP-Flex Services 
Application APIs.  Firmware is organized by function.

* [**Opl1000**](opl1000/ReleaseNotes.md)
* **StmBootloader** -- Latest versions of STM Bootloader.
* [**StmFirmware**](StmFirmware/ReleaseNotes.md) -- Latest versions of STM Firmware with the Flex platform installed.

The tag firmware contained here can be directly installed on the tag using the Nali-N100 Programmer.  These releases are intended 
for development and production use only.  Do not use these binaries with any other programmer other than the ones
available in this sdk.


---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
