### v1.0.0.20a -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 21-05-04
1) Implemented Out of Network (OON) features support.  Tag now caches messages for rebroadcast after rejoing network.
1) Reworked LoRa Wan Controller implementation to improve network handling.
2) Fixed bug sending only confirmed messages, new implementation now supports uncofirmed By
 default confirmation for location measurements are managed by the LinkCheck module.
3) Further stablized Archiving implementation fixing memory leaks.
4) New implementation of the radio uplink processing should improve consistency and reliability.
5) Increased Heap from 2K->4Kbyte.
6) Reworked Opulink power and UART handling to improve edge-cases.
7) Improved Archive subsystem stability
8) Improved core tasking implementation
9) Fully implemented timer support for CP-Flex application API (up to 4 timers).
10) Fixed 868MHz restriction bug occuring in the v1.0.0.  Forcing standard transmission delay interval under these cases.
11) Fixed Battery non-notification of charge complete. Now reports charge complete once again.
12) Added support for payload size change and more information about uplink.
13) Fixed network timeout issue prematurely giving up and rejoining under special cases.  
14) Revised RTC backup/softboot implementation
15) Enabled on-the-fly archive syslog commits
16) Revised 868Mhz restrict behavior.  Resets radio subsystem after 5 restricted Join attempts.
17) Improved radio subsystem reset sequence.
18) Revised RTC backup to always restore with new backup process.
19) Improved LoRa WAN MAC processing task.
20) Revised MLME Status and JoinNetwork logic.
21) Removed JOIN_WAIT LoRaWAN state.
22) Added DEVEUI to boot banner.
23) Revised task initialization and IRQ sequencing.
24) Bug Fix LoraMac process signaling to wake processing task instead of state task.
25) Added Cloud stack time synchronization for devices that lose their time.
26) Implemented updated messaging identifiers to specify messages that are current, delayed, and immediate uplink (for time synch).
27) Added Reset message.
28) Added App Log Telemetry implementation for sending app log messages to cloud.
29) Added System Log Telemetry uplink support for Error, Alert, and Warnings, can be turned by CP-Flex Apps.
30) Added Fatal error restart when Pawn virtual machine fails.
31) Changed max battery to 99% and send event when Charge Complete (100%).   This makes the readings more intuitive.
32) Fixed RTC Time correction algorithm.
33) Fixed NVM QuickWrite bug.
34) Added new TimeMangement, Random, and TimeCheck API's.
35) Added NVM Flex Error Handler mechanism.
36) Uplink Applogs, messages marked as Critical or High Priority are sent as Confirmed messages.
37) Tighten delay tolerance to 60 seconds to improve time sync accuracy and hysteresis, which could caused endless synch if delay +  one-way bias is ~ 5 minutes.
38) Bugfix: Fixed encoding problem with Application Log Uplink messages.
39) BugFix: AdjustTime algorithm improved to handle edge cases consistently.
40) Added battery percentage smoothing to reduce the effect of OPL voltage pulldown and provide more consistent sampling. Three (3) 
 sample box-car filter used.


### v0.10.0.03 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 21-03-26
Bug Fixes:
1) Added SysLog Archive FlexAPI interface.
2) Opulink Power Fault now sends dissconnect event once (instead of every timeout).
3) Fixed Kernel Overwrite/interrupt context switching issues, causing tasks to inadvertently suspend indefinitely. This affected
   communications, location, and interchip communications.  Causing the device to appear crashed when not really.
4) Implemented Archive infrastructure and updated CPFLEX API to support version v1.1.0.0a or later.
5) Implemented Archive configuration and download support for Nali Programmer (version v1.4.0.0 or later).
6) Fixed MLME Schedule Uplink Behavior.
7) Fixed additional instabilities in multi-tasking core.
8) Added OPL crash detection. STM will release power lock status if OPL locks up due to BLE memory leaks.
9) Restrucutred OPL<->STM Command Definitions (requires corresponding OPL Update)
10) Added Confirmation Support for Archive System Log Config Command
11) Streamlined kernel implementation to clarify use model to avoid common bug issues.
12) Restructured OPL State management to resolve nagging stability, communication issues, and unreliable booting.   

### v0.9.9.2_a -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 21-02-25
Bug Fixes:
1) Added Flex-API SysReset
2) Reworked Join control logic to improve reliability
3) Handle busy conditions by rejoin after 15 successive busys.
4) Fixed bug in Lorawan controller to improve reconnect.
5) Implemented BLE Proximity Detection Engine Alpha prototype.

### v0.9.6.0 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-12-05
Bug Fixes:
1) Added power status reporting when fully charged.
2) Updated Battery Voltage Algorithm
3) Lorawan join waits 2 second for join after boot from reset.
4) Fixed bug in Lorawan controller to improve reconnect.
5) Implemented BLE Proximity Detection Engine Alpha prototype.

### v0.9.4.5 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-10-08
Bug Fixes:
1) System crashes when out of heap: malloc not returning NULL when heap is full.
2) Sequence fragmentation into packets did not stop when transmit queue is full.
3) Transmit queue is improperly cleared leaving it non-functional.  This occurs when messages are not able to 
   be sent within 30 seconds.

### v0.9.4.2 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-09-30
1) Fixed production programming issue preventing identities from being stored.   
2) Stabilized Flex App initialization causing STM to crash during reset.

### v0.9.3.2 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-07-19
1) Added additional support for BVT Testing.
2) Fixed memory leak and button issues when transmit runs out of buffers. Should be more stable now.
3) KP-143 Device communication issue dropping last character in buffer when going to sleep.  Now makes sure buffer is clear before sleeping.

### v0.9.2.4 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-06-22
1) Fixed button issues causing mis-interpretation and potential power glitching.
2) Provided firmware version for CN470 - Tencent networks.

### v0.9.1.3 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-05-04
1) KP-130, KP-131, KP-92 Completed
2) KP-54, KP-66 Added NVM and Archive platform functions.  Ready for Flex APIs.
3) Reworked LoRa Telemetry join and communication infrastructure.  Now supports both
   confirmed and unconfirmed MCPS message indication, messaging priority, and rejoin.
4) Implemented support for LoRa network status (see ped_tracker implementation for example.)
5) Improved LoRa network monitoring efficiency likely providing a slight improvement on battery life.
6) Limited message maximum transmit rate to 5 seconds to reduce busy wait and needless thrashing.  Still working in setting carrier limits.
7) Fixed bug in CPFlex print function improperly formatting negative integers.

### v0.9.0.1 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-04-13
1) Updated Telemetry support to enable downlink of data.
1) Added Flex APIs to support downlink polling, see example implementations in TelemMgr.p found in ped_tracker or tag_demo.
2) Simplified task structure to increase memory efficiency.  
3) Increased Dynamic Heap memory for buffers to 2K to address increase memory usage downlink and uplink at same time.

### v0.8.0.4 -  Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-03-25
1) Improved power management monitoring with OPL.
2) Stabilized communication between STM and OPL
3) Fixed power state confusion on STM reset when OPL is powered.
4) Fixed multi-packet indexing issue.


### v0.7.0.1 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-02-11
1) KP-102 -  Added Infrastructure to execute script functions from BVT toolsets.
2) KP-96 - Extended interchip communication.
3) KP-120 - Fixed "bricked" STM behavior due to OPL Power being left on after charger off + Low/Depleted Battery Scenario).

### v0.6.2.7 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 20-01-08
1) KP-118 - Merged KP-108 into master so it can work.   Was causing int32 messages to not be interpreted correctly.
2) KP-111 - Fixed crashing issues with Ped Tracker script, which was caused by underlying instability in the callback handle infrastructure.
3) KP-107 - Updated power percetage reader to accurately report remaining power. Wrote calibration routine to try and stabilize differences between Eval3 and Proto3.
4) KP-108 - Updated CP-Flex telemetry API to support simpler use case.


### v0.6.1.1 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 12/19/19
1) KP-94 - Reworked alarm infrastructure to use Alarm B, Alarm A is reserved by LoRaWAN driver.
2) KP-95 - Fixed Alarms quitting after 16 events.  Perf_test script exhausting handles instead of recycling.  Firmware logic has been fixed to allow more permissive use of handles by script implementation, automatically recycling/reusing existing handles.

### v0.6.0.3 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 12/14/19
1) Fixed RTC Persistence between reset.
2) Fixed GPIO Pin 10 leakage current issue (KP-93)
3) Fixed power event detection (KP-63).
4) Bug fix, dropped messages after reboot.  Fixed by resetting UART hardware receive buffer.
5) Added debug trace information to report start timecode.

### v0.6.0.0 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 12/8/19
1) Replaced internal communications infrastructure.
2) Added BVT functions.
3) Implemented Power charge / discharge functions.
4) Releases now available for US915 and CN470.  Currently testing US915.
4) Bug fixes

### v0.5.3.4 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 13-Nov-2019
1) Fixed PA_11 pin issue.

### v0.5.3.3 - Interim Release for Eval3/Proto3 (Rev3) ###
Release Date: 11-Nov-2019
1) Updated GPIO Pin mappings on this release to match new Eval3 and Proto3 board layouts.

### v0.5.3.2 - Interim Release ###
Release Date: 13-Oct-2019
1) Rebuilt after code  review. Trying to assess if power loss due to bad STM build.

### v0.5.3.1 - Interim Release ###
Release Date: 08-Oct-2019
1) Turned off NO_DEEP_SLEEP compiler option 

### v0.5.3.0 - Interim Release ###
Release Date: 07-Oct-2019
1) Added functions to read DevEui and AppKey configuration data via OPL1000.
2) Updated select GPIOs to reduce power consumption to less than 30 uAhrs when asleep.

### v0.5.2.0 - Interim Release ###
Release Date: 26-Sep-2019
1) Fixed WiFi Scan issue causing double press.

2) Added Power API (low-level) to read voltage data.  Still need to implement flex API.
3) Added Reset capability.  Push any button continuously for more than 15 seconds and device will reboot.

4) Forced LEDs off when completing count or blink mode.

### v0.5.1.3 - Interim Release ###
Release Date: 15-Sep-2019
1) Stabilization features to make location reporting work.
2) Many bug fixes.

### v0.5.1.2 - Interim Release ###
Release Date: 12-Sep-2019
1) Now puts the OPL to Sleep
2) Implemented work-around to allow multiple WiFi scans.
3) Implemented Accelerometer functions
4) Miscellaneous Bug Fixes.

### v0.5.1.1 - Interim Release ###
Release Date: 9-Sep-2019
1) Enable Sleep state
2) Reworked LED signalling so it works as expected.
3) Updated platform task management so it is a bit more efficient.
4) Miscellaneous Bug Fixes.

### v0.5.1.0 - Interim Release ###
Release Date: 5-Sep-2019
1) Fixes Checksum Issues