# A2 OTA Firmware Release Notes#

All binaries stored here are packaged and ready for download.  These binaries support
OTA download functions over BLE.

### V0.9.0.3 - 210326 ###
1)  Fixed upstream BLE memory leak
2)  Changed BLE Advertising Rate Window to 6.66Hz <-> 9Hz
3)  Added Confirmation Support for Archive System Log Config Command
4)  Restrucutred OPL<->STM Command Definitions (requires corresponding STM Update)
5) Fixed STM<-->OPL Interchip communication issues when servicing multiple threads.
6) Added Archive Upload and System Logs support for Nali Programmer BVT API.
7) Changed BLE GATT server advertising rate to 250ms.  May make this even faster later.
8) Added Host->OPL Throughput Test Loopback & OPL->STM Simulated Archive Testing Modes
9) Changed UART TX to Evt Queue implementation
10) Added Memory Utils for Stack/Heap Eval

### V0.8.3.9a - 210216 ###
1) Added redundant OnCharger state checking.

### V0.8.3.8a - 210209 ###
1) Changed GATTSERVER_Locked advertising rate from 750mS-1s to 250ms-250ms range.

### V0.8.3.7 - 201028 ###
1) Refactored firmware to support 2-minute forced GATTSERVER mode when charger is connected.
2) Fixed bugs related to proximity detection.

### V0.8.3.5a - 201023 ###
1) Fixed bug in prior release GATTSERVER locking feature.

### V0.8.3.4a - 201023 ###
1) Refactored firmware to support 2-minute forced GATTSERVER mode when charger is connected.

### V0.8.3.3a - 201009 ###
1) Rearchitected firmware to support multiple BLE configurations without reset.
	a) GATT server mode
	b) BLE scanning mode
	c) BLE beaconing mode.
2) Implemented Proximity Detection Engine to support beaconing and scanning for proximity information.
3) Enabled WiFi and Beaconing to work while the device plugged into charger as long as it is not connected to a BLE GATT client.   
4) OPL chip will always reset whenever the charger is applied to ensure that it can be accessed externally.  This is needed to prevent bricking.   


### V0.8.2.1 - 200909 ###
1) BUGFIX - Reworked BLE Gatt Server on write received to optionally support both write with response or write without response.  Write without response is used by updated Android tools to provide faster updates.

### V0.8.1.10 - 200717 ###
1) KP-141 Pt1.- Added BVT testing mode.
2) KP-115 - Added Software Profile set/get functions and last time synch.
3) KP-143 - Fixed boot modes to allow restoration of normal operating mode after test.
4) Added set/get functions for MfgId.

### V0.8.0.4 - 200325 ###
1) Implemented selectable scanning functions (BLE, WiFi, coarse, medium, precise.
2) Scanning is only supported when charger is disconnected.  This is due to the charger enabling firmware updates via BLE communication.
3) Reversed the order of the MAC BLE bytes when scanning to be consistent with industry standard.
4) Disabled ack requireing during bootup when placed on charger, was causing STM to be reset 3 times.  Improvement in communications has mostly 
   obsoleted the need for this.

### V0.7.0.4 - 200211 ###
1) KP-96  - Reworked firmware updates to work using UART and BLE.
2) KP-102 - Added improvements to support BVT test framework.
3) KP-117 - Restructured Message framework to support UART and BLE Communications with same command processing infrastructure.
4) KP-113 Fixed OPL MAC address configuration reporting bug. 
5) Changed BLE name to Nali-N100.
6) Updated firmware bootloader code to improve reliability during uploads.

### v0.6.1.3 - 200102 ###
1) KP-112 - Fixed improper formatting of WiFi measurement data.
2) KP-108 - Fixed overwrite of Mac addresses in OTA version.
3) KP-114 - Fixed STM bricking issue after OTA BLE Script update.

### v0.6.0.5 - 191214 ###
1) Fixed BLE OTA Config Read issue.  Now issues request for configuration information at the time of the request.
2) Added STM Bootloader Flash write capability to BVT programming.
3) Fixed BLE and WiFi MAC persistence issue.
4) Tuned wakeup delay between OPL and STM to reduce potential for drop-outs
5) Updated  BLE OTA ReadSoftwareConfig information to pull correct Mac addresses.
6) Added additional error checking on MAC address programming.
7) Increased STM wakeup delay to account for 56uS wakeup period. Was missing some messages.

### v0.6.0.0 - 191208 ###
1) Implemented New Comm interface and support for Rev3 layout.
2) Implemented BVT functions to support serial port programming of all device functions
3) MAC address program is currently only saved until reset.  Working on currently.

### v0.5.1.1 - 191008 REV2 ###
1) Clean build uploaded by Derek after verification.  

### v0.5.1.0 - 191007 REV2 ###
1) Added support to read configuration data from BLE interface
2) Renamed full name of the device to be Kiata LoRa Tracker M100

### v0.5.0.4 - 190927 REV2 ###
1) Fixed bad initialization code in bootloader preventing OPL from controlling STM.  
   Tested with various A2 devices.

### v0.5.0.3 - 190926 ###
1) Merged latest updates from 2.10 release of A2 SDK.
2) Fixed double checksum error issue.


### v0.5.0.2 ###
1) Adds negative sign fix to correct WiFi Observation data issue.
