# Flex Mobile Tools for CT1XXX (and Nali) Devices

[Click here for release notes.](ReleaseNotes.md)

Flex Mobile tools is an Android based application for managing CT1XXX trackersDownload the APK to your
Android device and install.  

Follow the instructions in this video https://youtu.be/t3HvwrnGrtE.  

---
*Copyright 2019-2023, Codepoint Technologies,* 
*All Rights Reserved*
