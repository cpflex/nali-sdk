CT1XXXX/Nali Android Mobile Tools Release Notes

### v1.0.3 231122 - Android APK###
Release Date: 11/22/2023
1) Updated default URL to point at global.cpflex.tech.


### v1.0.2 230908 - Android APK###
Release Date: 09/08/2023
1) Updated to support latest changes to Tracker firmware.

### v1.0.1 230708 - Android APK###
Release Date: 07/08/2023
1) Many Updates since previous.



### v0.9.4.221003 - Android APK###
Release Date: 10/03/2022
1) Misc. Bug fixes



### v0.9.2.2111081 - Android APK###
Release Date: 11/08/2021

1) Misc. Bug fixes

### v0.9.1.210821 - Android APK###
Release Date: 8/20/2021

1) Added Support for N100/N110 devices.
2) Added some exception handling to improve stability.

### v0.7.2.210201 - Android APK###
Release Date: 2/1/2021

1) Fixes clean install crashing issue when account not specified (KP-183).
2) Added version information to settings dialog (KP-192).

### v0.7.1.0 - Android APK###
Release Date: 12/14/2020

Release removes backward compatibility with PoC servers.  Do not use this version if needing to interact with PoC servers.

1) Settings now has account selection based on API key.   To use provisioning and other features requires specification of an API key and account.
2) Added Registration Confirmation step to verify account, key, and network provider before registering.
3) Fixed crashing bug when deleting devices.
4) Devices are now filtered by account. Only unregistered and account devices are shown.  Others remain in local database, but are not visibile until user
   logs into that specified account.
5) Updated synchronization logic to not overwrite important server settings.


### v0.7.0.3 - Android APK###
Release Date: 12/7/2020

Release implements support for device provisioning with CP-Flex provisioning services.  This app
implements API Key and other security protocols to support the production CP-Flex services.   

1) Added Provisioning Features to register and synchronize devices with CP-Flex services.
2) Added API key support for CP-Flex Hub application registry to access production CP-Flex services.
3) Added API key and Provisioning URL settings in settings dialog.
4) Updated local database to store account and registration information.  This requires reloading devices.
5) Added hard reset of device after update or new application install.
6) Various bug fixes.
7) Fixed STM attributes programming issue that was causing the EU version to be programmed on US version.
8) Fixed Account/Registration Key selection bugs on multiple accounts.

### v0.6.2.0 - Android APK ###
Release Date: 10/14/2020
Minor update fixing a number of UI related issues.
1) Now updates device data in phone database after installing or updating flex applications
2) Cached application data for faster display once loaded.
3) Now updates Applications page automatically when settings changed.

### v0.6.1.0 - Android APK ###
Release Date: 9/25/2020
1) Implemented deletion of Devices on Devices page.
2) Now updates device information after editing device name.
3) Added Path field to Application items.

### v0.6.0.3 - Android APK ###
Release Date: 9/09/2020
1) Integrated with CP-Flex Hub
2) Added support for reading application package information 
3) Added support for application selection.
4) Simplified Activities and UI to remove manual configuration components.
5) Updated documentation.


### v0.5.2.1 - Android APK ###
Release Date: 4/29/2020
1) Added Reset Device Task.

### v0.5.1.0 - Android APK ###
Release Date: 3/8/2020
1) Added reading of profile to retrieve LoRa and other identifiers.
2) Implemented swipe right/left functions on devices view.  Delete not implemented yet.
3) Removed log fields in settings, since they are not used right now.
4) Added devices profile detail by swiping right on devices view.
5) Fixed various bugs.


### v0.5.0.0 - Android APK ###
Release Date: 3/6/2020
1) Initial release with basic programming functions and UI model implemented

