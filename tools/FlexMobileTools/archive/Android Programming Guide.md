# Managing Nali Devices with Nali Mobile Tools


From time to time Codepoint will release updated firmware for the Nali tags, 
and you may also write your own tag application in FlexScript. The LoRa Tag SDK includes both a windows based and Android based programming tool that allows you to program new firmware and/or application software into your tag(s).


## Installing the Android App
You must first install the android application onto your Android device. This is typeically accomplished by downloading the `.apk` file located in this directory to the `Downloads` folder of your Android device. Once downloaded, follow the standard Android procedure to install the app on your device.		
## Running the App
The icon for the app is shown in the screenshot below. Tap the icon to launch the app.

![](./images/Screenshot_1.jpg)

The first time you run the app, it will place you in the BLE scanner mode. Here the app will show you any Nali100 tags that are active *and* plugged into a charging cable. A tag *must* be plugged into a charging cable before it can be programmed.

![](./images/Screenshot_2.jpg)

If you do not see the device you expected, you can tap the `RE-SCAN` button to force the app to re-scan for Nali devices.

You must first add any found devices to the internal database of the app. To do this, long press on a device, and you will see a `+` icon appear on the bottom right of the screen. Press the icon to add the device to the database. Do this for all devices found that you wish to program.

![](./images/Screenshot_3.jpg)

Once the app has run once and added tags to its database, the app will automatically start up in the devices screen from then on. It will show you the list of devices it has stored in its internal database, regardless of whether they are plugged into a charging cable or not.

![](./images/Screenshot_4.jpg)


On the devices screen, you can delete a device from the database by swiping  swiping left on the device then selecting `Delete`.

![](./images/Screenshot_4a.jpg)

To view the profile for a device (DevEUI, AppEUI etc), swipe right on the device and you will be presented with the relevant information for that device.

![](./images/Screenshot_4b.jpg)
![](./images/Screenshot_4c.png)

## Working with Devices

### 1. Select Devices to Participate in an Activity
To select a device for an activity, press and hold on the panel of the device.  To deselect, tap 
the device panel again.  Once one or more devices are selected.  click the add activity icon or clear activity icon, which adds or removes selected
devices from the activity queue. The two icons can be found at the lower-right of the screen when devices are selected.

![](./images/Screenshot_7.jpg)


### 2. Select CP-Flex Application to Install 
If a device needs to have a different application installed, first select the 
application in the Applications view. Skip this section if updating or performance other device
activities.

![](./images/Screenshot_13.png)

### 3. Execute an Activity
Once the devices and applications are selected and the devices show they are within BLE range, use the
Activities view to execute various activites with the devices.

Once you have the devices to update selected, press the menu icon and select `Activities`. 

![](./images/Screenshot_8.png)

This will bring up the update screen. First, choose an activity from the dropdown menu.  

![](./images/Screenshot_9.png)

Once you have chose an activity to perform, the update icon will turn green. Pressing the update icon will begin the update process. 

![](./images/Screenshot_10.jpg)

Devices are updated squentially, so you will see progress bars for each device in the update list. The app will then go and update each device one by one. 

![](./images/Screenshot_11.jpg)

Upon successful completion, and green check will be placed next to the device. 

![](./images/Screenshot_12.jpg)

If you have selected 'update firmware', this activity updates all 3 firmware files. If you find you are having problems with updating a particular piece of firmware (e.g. OPL, STM or Flexscript), you can select just that activity from the dropdown and keep retrying on the function until you succeed. Then you can move on to programming the next piece of firmware.

If you encounter errors while programming, try programming again. We have found that for best performance, your Android device should be at least 1m away from the tag itself.


## Changing Setings
The 3 dots (aka the kebab icon), provides access to application settings.   These are advanced settings
that will not need to be adjusted under ordinary use.   

![](./images/Screenshot_6.png)

The current settings allow for specification of an alternate CP-Flex Hub URL and filtering of application versions.
By default only the latest applications are shown.

---
*Copyright 2019-2020, Codepoint Technologies,* 
*All Rights Reserved*
