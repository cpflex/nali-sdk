# Tools 

This folder contains programming tools for installing firmware and scripts.

The primary tool for managing programming for the Nali-N100 tag are as follows:
- [**Cora Tracker Programmer**](CoraTrackerProgrammer/README.md) -- Windows USB / BLE Developer Only Programming tool (Version 0.7.0 software and later.)
- [**Flex Mobile Tools**](FlexMobileTools/README.md) -- Android based programming tool (Version 0.7.0 software and later.)

---
*Copyright 2019-2023, Codepoint Technologies,* 
*All Rights Reserved*
