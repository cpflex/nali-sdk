# Upgrading Firmware in your Nali Tags
From time to time Codepoint will release updated firmware for the Nali-N100 tags, and you may also write your own 
CP-Flex application. The Nali-N100 SDK includes a windows based programming tool that allows 
you to program new firmware and/or application software into your tag(s).

## Installing the Nali-N100 Programmer
The first thing to do is install the Nali-N100 Programmer that came with the version of the 
SDK you downloaded from gitlab (https://gitlab.com/kiata/partners/nali-sdk.git). First use the add/remove programs command under the 
control panel of your Windows PC to remove any earlier version of the programmer 
you may have installed on your computer.

Then run the '`setup`' program under the directory `nali-sdk/tools/NaliN100Programmer`

![](./images/Screenshot-2.png)

## Configuring the Nali-N100 Programmer
Once you have installed the programmer, use the Windows search function to run it:

![](./images/Screenshot-3.png)

You will then see the default programmer screen

![](./images/Screenshot-4.png)

Before using the programmer, it is required to configure the settings so that it can run effectively and produce the 
intended results.   The programmer is capable of doing factory programming however, it is strongly recommended that only
expert users utilize that capability.  Most users will use the tool to perform updates, get device information, and install
new application packages.   

For performing factory programming or rework you will need to configure the device identity settings.  See 
[Setting Device Identifier Values]. 

## Configuring the Log Files.
To capture logs of programming activities, specify the log files in the *Programming...* menu under *Settings*.  

![Programming](images/Programming.png)

The Logging section specifies the log files for saving both detailed and summary logs.  If they are checked (left side), then
the logs will be saved during programming, if not checked logging is disabled.  To specify a log file click the '...' button at the 
right side, which will prompt the user for a file path to store logs.  As a matter of good practice logs should be save
to their own folder named something like 'logs'.  Use of the this tool repeatedly, will create lots of log files.

The detailed log files capture all the log information for a particular programming activity.  The summary log file captures 
only the summary information, which appended each time a programming activity is executed.

The Custom Package group defines a custom package of firmware, which is typically used for development.  It is recommended
that these not be set by casual users of the tool.  The programmer can pull packages of firmware from the CP-Hub, which 
will populate these fields automatically.

## Preparing to Start a Programming Activity
With initial configuration complete, the programmer is nearly ready to run.  Now on the main window a few pieces of 
critical information are required.   Follow the steps below.
1. **Specfiy the default profile** -- For certain activities, a default profile will be required.  Determine which device
 profile the attached device is and make sure it is selected in the drop list next to the Start button.  The choices are 
   1. *CN470-Generic* -- Standard profile for devices supporting China 470 MHz LoRaWAN profiles.
   2. *CN470-Tencent* -- Tencent profile for devices supporting Tencent China 470 MHz LoRaWAN network.
   3. *US915-Generic* -- Standard profile for devices supporting U.S./Canada 915 MHz LoRaWAN profile.
   4. *EU868-Generic* -- Standard profile for devices supporting Europe 868 MHz LoRaWAN profile.
2. **Specify the Appliation Package** -- Click the *Packages...* button to select a CP-Flex Application package for installation.
 Depending on the programming activity this may be required.
3. **Specify the Connection** -- The programmer can support USB COM or BLE connections.  For non-factory use, the connection
 will likely be BLE.   Make sure your Nali-N100 is placed on the charger so that BLE communications are enabled.   If you do 
 not see your device try clicking the *Refresh* button.

The mac address for your tag will be located on the back as shown 
All tags should have a sticker on them with barcode labels for both the serial number and BLE Mac address

![](./images/tag.png)

**NOTE** When selecting profiles, be sure your device is capable of supporting it.  
If you are updating the firmware to the latest contained within the SDK, check all the boxes shown. If you are only updating the application script in the tag, you only need to check the box for CP-Flex Script. In all cases though, you should always check the box for updating the UTC time.

Selecting the application package shows a selection form as follows.

![Select Package](images/SelectPackage.png)

The list box on the left side shows the available packages for your tag based on the default profile.   Choose the package 
of interest.  In most cases you will want to select packages with tags marked as latest.   This will ensure that you
keep your device up to date.  As shown the ped_tracker:latest package is selected, which will install the latest pedstrian 
tracker application for Nali N100 Rev 3.   

Be sure to save your configuration by selection *File.Save As*.  This will prompt for a filename and location.  Save your configuration
so that you can readily retrieve it for the next session.  The programmer automatically loads the last configuration
saved.  You can check the File.Recent for a list of other configurations saved previously.

## Start a Programming Activity

With the default values specified, you are now ready start an activity.  Select the activity in the drop list as shown 
below.

![Select Activity](images/SelectActivity.png)

The activity list contains a variety of predefined activities, some of which are defined as follows.

- **Synchronize Time** - Synchronizes the device time with the current PC time.
- **Reset Device** - Resets the device to normal mode.  
- **Get Device Information** - Returns device identification and profile information.
- **Update Firmware** - Updates device firmware if a newer version is available.
- **Install Package** - Installs the specified application package.
- **Validate and Update N100 Identities** - Verifies and updates if needed the device identifiers to comply with latest requirements.
- **Reconfigure Existing** - Reconfigures devices with pre 0.9.3 firmware by updating firmware, identities, and profile to comply with latest requirements.   New tags
 will not require this.  This rquires a valid manufacturing identifer.  See [Setting Device Identifier Values].
- **Factory Program Device** - Performs an initial factory program on the device given the default profile and application package.  This
- requies valid identities be specified in the identifier settings. See [Setting Device Identifier Values].
- **Test Device** - Performs a basic functional test of the device to validate normative function.
- **Ping Device** -  Connects with the device and sends four ping messages.  Used to verify that basic communication functions
 are working with the device.
- **RF Performance Verification** - Performs an RF test of LoRaWAN, WiFi and BLE.  Only available for USB based connections.

Select the activity of interest.  For example, select *Get Device Information*.   Then click the start button.  If the 
sequence starts you will see log output similar to the following:

```
11:56:35.0 INF - Connecting BLE device (C0132BCF57F5, key= comm_default_client)
11:56:35.0 INF - Adding BLE Service Connection C0132BCF57F5:AAAA with global key comm_default
11:56:35.0 INF - Created Connection: C0-13-2B-CF-57-F5, Nali-N100
11:56:35.2 INF - Connecting BLE Service: AAAA
11:56:35.8 INF - BLE Device Connected
11:56:37.4 INF - Getting LoRa Identifiers
11:56:37.5 INF - Getting Mac IDs
11:56:37.7 INF - Getting Manufacturer ID (MfgId)
11:56:37.8 INF - 
Device Identifiers
{
  mfgid   : Nali-N100-2038100013
  deveui  : D8-8B-4C-3B-A1-D3-10-33
  joineui : D8-8B-4C-34-03-00-00-00
  appkey  : 20-14-4D-80-2F-E7-05-4C-D8-AA-B4-D9-06-59-AB-94
  ble mac : C0-13-2B-CF-57-F5
}
11:56:38.1 INF - 
Device Profile
{
  "name": "CN470-Generic",
  "package": "//hub.cpflex.tech/nali/applications/ped_tracker:v0.9.1.4",
  "devicetype": "//hub.cpflex.tech/device-type/nali/n100/v2:rev3",
  "lastupdate": "2020-07-23T20:00:05.2219395-07:00",
  "attributes": {
    "frequency": "470MHZ",
    "region": "China (CN)"
  }
}
11:56:38.1 INF - Terminating
11:56:38.1 INF - Disconnecting BLE device (C0132BCF57F5, key= comm_default_client)
11:56:38.2 INF - BLE Device Disconnected
11:56:38.2 INF - Complete: Success
```

If it doesn't work, you may need to reconfigure your device.

You have now successfully updated the firmware on you Nali100 tag. You may remove the tag from charging cable and begin using it.

## Setting Device Identifier Values
If using the programmer for factory programming or rework, you will need to specify the identification settings. Select
*Identifiers ...* in the settings menu to open the Device Identification Settings dialog.  A screenshot of the 
dialog is shown below.

![Identifier Settings](images/IdentifierSettings.png)

To create a device identity requires specification of the following values:

1. **LoRaWAN Join/App EUI (hex 8 bytes)** -- an eight byte hex value specifying the LoRaWAN network application access key.
 This is required for the device to join an application group on the LoRaWAN network.  This value is determined by the 
 application provider.  
2.  **LoRaWAN Device EUI Identifier (8 bytes)** -- an eight byte hex value specifying the unique device identifier.  This is
 specified as a range of allowed values with a start and end value.  The index defines which value will be used next when
 programming. Proper ranges of values should be obtained from the LoRaWAN network provider, Codepoint, or manufacturer.
3. **BLE / WiFi MAC identifier (6 bytes)** -- a six byte identifier used to identify the device when connecting via BLE or WiFi.  This is
 specified as a range of allowed values with a start and end value.  The index defines which value will be used next when
 programming. Proper values should be obtained from Codepoint or the manufacturer.
4. **Manufacturer (MFG) Identifier - Identifier Format** -- The format string used to construct  Manufacturing Identifiers.
 The format can be a combination of any characters, current date, and a number whose range is defined by the start, end, and index
 values.  The format string is compliant with C# string.Format(...) function, where the two arguments {0} is the number
 and {1} is the current date time.   The format number and date time formats can be specified per C# string.format specifications.
 The default value for the identifier is Nali-N100-{1:yyMM}1{0:00000}, where the current year and month are concatenated with the number
 value with 5 digits.
5. **Auto Increment Identifiers** -- If true (default), the indexes will auto increment whenver a new device identity is 
 created.  In general, this should be true.  

The button *Reset Indexes* will set all the indexe to zero.


---
*Copyright 2019-2020, Codepoint Technologies, Inc.* 
*All Rights Reserved*
