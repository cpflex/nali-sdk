# Cora CT1XXX / Nali N100 Development Programming Tool
## v1.10.5.0 230929
1) Many updates and improvements supporting new architecture.

## v1.10.3.1 230708
1) Many updates and improvements supporting new architecture.

## v1.8.10.0 - Nali N100 Programmer ###
Release Date: 22-10-13
1) Miscellaneous

## v1.5.1.1 - Nali N100 Programmer ###
Release Date: 21-11-08
1) Updated N100 profiles to distinguish protocol versions.  Now supports 
   Coralink.
2) Added force configure activity specification.

## v1.5.0.1. - Nali N100 Programmer ###
Release Date: 21-08-12
1) Added support for reading STM chip UUID as part of 
 supporting CoraLink.  This is a WIP.
2) Restructured firmware set construction to support multiple devices including N100/N110.
3) Added stabilization code to ensure older firmware continues to work.

### v1.4.1.1 - Nali N100 Programmer ###
Release Date: 21-03-17
1) Fixed BLE uplink transmission bug truncating long packets.  This was affecting hardware profile registration.
2) Fixed category, priority, and source labelling bug in CSV files.

### v1.4.0.1 - Nali N100 Programmer ###
Release Date: 21-03-14
1) Resetting BLE before each scan.
2) Updated BLE Framework Library ot V7.10.10.0 
3) Added support for Configuration and reading N100 archive and system logs.
4) Misc. UI fixes and tweaks.
5) Improved the set system log activity.

### v1.3.3.0 - Nali N100 Programmer ###
Release Date: 1/7/2021
1) Added APIKEY support for accessing production Hub services at http://global.cpflex.tech/hub/v1.0.  
2) Fixed App definitions loading exception.
s
### v1.3.2.1 - Nali N100 Programmer ###
Release Date: 10/1/2020
1) Fixed persistence issue on overwrite and increment identities  settings.
2) Fixed auto increment settings transfering to/from identities dialog.
3) Enhanced package selector to filter latest and make the page controls actually work.

### v1.3.1.2 - Nali N100 Programmer ###
Release Date: 9/11/2020
1) Reworked BLE communications to support changes in Nali-N100 BLE characteristics.  Changes remain backward compatible.
2) Cleaned up Refresh Connections functions to be disabled while scanning.   Provides less confusion.

### v1.2.1.0 - Nali N100 Programmer ###
Release Date: 8/14/2020
1) Fixed bug in Generic-EU868 profile, preventing proper resolution of firmware.

### v1.2.0.4 - Nali N100 Programmer ###
Release Date: 7/24/2020
1) Now supports Activities
2) Now supports CP-Flex Application Packages
3) Now supports Hardware profiles
4) Improved GUI on Nali Programmer.

### v1.1.1.0 - Nali N100 Programmer ###
Release Date: 4/30/2020
1) Fixed timing issue when set identification.
2) Added LoRa ID verification during confiugration of identifiers.
3) Shortened the reset time for time synchronization.

### v1.1.0.3 - Nali N100 Programmer ###
Release Date: 2/17/2020
1) Implemented support for v0.7.0 BLE / USB support.
2) Harmonized with changes in BVT architecture.
3) Optimized pipelining for both serial and BLE based programming now that both are supported.
4) Fixed BLE programming sequence when OPL is included.  Can do a full cycle now.
5) Added additional validation logic to verify ranges when auto increment is enabled.

### v1.0.1.0 - LoRa Tag Programmer ###
Release Date: 12/19/19
1) Implemented Bootloader programming mode.
2) Implemented Set Epoch programming mode.
3) BUGFIX:  Added protections to handle corrupt or missing configuration duuring program load.
