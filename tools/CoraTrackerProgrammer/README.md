# Cora Tracker Programmer

[Click here for release notes.](ReleaseNotes.md)

This Windows application provides firmware update and provisioning functions for the Nali N100 tag.  It
supports both BLE and USB based connections.  The application is meant for development and factory 
programming. 

---
*Copyright 2019-2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
